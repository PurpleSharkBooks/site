<?php

// include_paths.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    $vendorDir . '/pear/archive_tar',
    $vendorDir . '/pear/console_getopt',
    $vendorDir . '/pear/file_archive',
    $vendorDir . '/pear/mime_type',
    $vendorDir . '/pear/pear',
    $vendorDir . '/pear/pear_exception',
    $vendorDir . '/pear/structures_graph',
    $vendorDir . '/pear/xml_util',
);
